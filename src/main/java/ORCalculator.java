import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;

public class ORCalculator {
    public final String mobileNumberErrorMessage = "Please enter a valid mobile number.";
    public final String mobileNumberXpath   = "//input[@name='phoneNumber']";
    public final String mobileErrorXpath    = "//input[@name='phoneNumber']/parent::div/following-sibling::p[@class='error-message']";
    public final String cityXpath           = "//*[text()[contains(.,'Where do you stay?')]]/parent::div/descendant::input[@role='combobox']";
    public final String cityErrorXpath      = "//city-auto-complete/descendant::p[@class='error-message']";
    public final String cityErrorMessage    = "Where do you stay? Field Required";
    public final String allMembersXpath     = "//app-add-member-btn//span[text()= '+']/parent::span/parent::div";
    public final String hiddenMembersXpath  = "//app-add-member-btn//span[text()= '+']/parent::span/parent::div[(@hidden)]/span/parent::div";
    public final String removeMemberXpath   = "//text-label//label[text()='memberName']/following::a[text()='Remove ']";
    public final String memberAgeXpath      = "//label[text()[contains(.,'memberName')]]/following::input[@role='combobox']";
    public final String memberCoverXpath    = "//label[text()='memberName']/following::select[contains(@name,'Cover')]";
    // private WebDriver driver;

    public void fillMobileNumber(WebDriver driver, String number){
        WebElement mobileNumber = driver.findElement(By.xpath(mobileNumberXpath));
        mobileNumber.clear();
        mobileNumber.sendKeys(number);
        mobileNumber.sendKeys(Keys.ENTER);
    }

    public void fillCity(WebDriver driver, String city){
        WebElement cityField = driver.findElement(By.xpath(cityXpath));
        cityField.clear();
        cityField.sendKeys(city);
        cityField.sendKeys(Keys.ENTER);
    }
    public boolean mobileNumberValidation(WebDriver driver){
        //check for all 10 digit numbers
        for(int i=1;i<=9;++i) {
            String number = "" + AttributeValueCreater.randomNumber(10, i);
            fillMobileNumber(driver,number);
            boolean result = validateMobileErrorMessagePresent(driver);
            if((i<6 && !result)|| (i>5 && result))
                return false;
        }
        return true;
    }

    public boolean cityFieldValidation(WebDriver driver){
        boolean result = false;
        driver.findElement(By.xpath(mobileNumberXpath)).sendKeys(Keys.ENTER);
        result = validateCityErrorMessagePresent(driver);
        if(!result)
            return false;
        fillCity(driver,"Nagpur");
        driver.findElement(By.xpath(mobileNumberXpath)).sendKeys(Keys.ENTER);
        return !validateCityErrorMessagePresent(driver);
    }

    public boolean validateMobileErrorMessagePresent(WebDriver driver){
        try {
            WebElement errorMessage = driver.findElement(By.xpath(mobileErrorXpath));
            if ( errorMessage.getText().equals(mobileNumberErrorMessage))
                return true;
            else
                return false;
        }catch(NoSuchElementException e) {
            return false;
        }
    }


    public boolean validateCityErrorMessagePresent(WebDriver driver){
        try {
            WebElement errorMessage = driver.findElement(By.xpath(cityErrorXpath));
            if ( errorMessage.getText().equals(cityErrorMessage))
                return true;
            else
                return false;
        }catch(NoSuchElementException e) {
            return false;
        }
    }

   public boolean checkAddedMembers(WebDriver driver, List members){
        List<WebElement> pageMembers = driver.findElements(By.xpath("//label"));
        Set<String> familyMembers = new HashSet<String>();
        for(WebElement e : pageMembers){
            familyMembers.add(e.getText());
        }
        if(familyMembers.containsAll(members))
            return true;

        return false;
    }

    public boolean checkNonAddedMembers(WebDriver driver, List Members){
        List<WebElement> allMembers = driver.findElements(By.xpath(allMembersXpath));
        allMembers.removeAll(driver.findElements(By.xpath(hiddenMembersXpath)));
        Set<String> nonAddedMembers = new HashSet<String>();
        for(WebElement e : allMembers){
            nonAddedMembers.add(e.getText().replace("+ ",""));
        }

        if(Members.containsAll(nonAddedMembers))
            return true;

        return false;
    }

    public void removeMember(WebDriver driver, String member){
        driver.findElement(By.xpath(removeMemberXpath.replace("memberName",member))).click();
    }

    public void addMember(WebDriver driver, String member){
        List<WebElement> allMembers = driver.findElements(By.xpath(allMembersXpath));
        allMembers.removeAll(driver.findElements(By.xpath(hiddenMembersXpath)));
        for(WebElement e : allMembers){
            if(e.getText().contains(member)){
                e.click();
            }
        }
    }

    public void setMemberAge(WebDriver driver, String member,int age) throws InterruptedException {
        WebElement memberAge = driver.findElement(By.xpath(memberAgeXpath.replace("memberName",member)));
        memberAge.clear();
        memberAge.sendKeys(age+"");
        memberAge.sendKeys(Keys.ENTER);
        memberAge.sendKeys(Keys.TAB);
    }

    public void setMemberCover(WebDriver driver, String member, int cover){
        String xpath = memberCoverXpath.replace("memberName",member);
        Select memberCover = new Select(driver.findElement(By.xpath(xpath)));
        memberCover.selectByValue(cover + "");
    }

    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "/home/hi.agrawal/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://devwebsite.amhi.in/b/c/optima-restore/individual");
        ORCalculator or = new ORCalculator();
      /*  System.out.println("Starting Mobile Number Validation");
        System.out.println("Mobile Number Validation: " + or.mobileNumberValidation(driver));
        System.out.println("Starting City Field Validation");
        System.out.println("City Validation: " + or.cityFieldValidation(driver));
        System.out.println("Starting Members Validation");
        String members[] = {"Self","Wife","Son","Daughter","Mother","Father","Father-in-law","Mother-in-law","Husband"};
        List<String> memberList = Arrays.asList(members);
        System.out.println("Non Added Members validation :" + or.checkNonAddedMembers(driver,memberList));
        System.out.println(" Adding Wife");
        or.addMember(driver,"Wife");
        Thread.sleep(2000);
        or.removeMember(driver,"Wife");*/
        or.setMemberAge(driver,"Self",23);
        or.setMemberCover(driver,"Self",1000000);
        or.addMember(driver,"Wife");
        or.setMemberAge(driver,"Wife",26);
        or.setMemberCover(driver,"Wife",500000);
    }
}
