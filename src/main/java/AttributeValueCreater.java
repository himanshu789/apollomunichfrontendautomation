import java.util.Random;

public class AttributeValueCreater {
    static 	Random 	rnd		= new Random();
    public static String randomString(int length,String startingCharacter) {
        StringBuilder random 	= new StringBuilder();
        if(startingCharacter!=null){
            --length;
            random.append(startingCharacter);
        }
        for(int i=0;i<length;++i) {
            char c 		= (char)(rnd.nextInt(26)+97);
            random.append(c);
        }
        return random.toString();
    }

    public static long randomNumber(int length, int StartingDigit) {
        StringBuilder number = new StringBuilder("" + StartingDigit);
        for(int i=1;i<length;++i) {
            number.append(rnd.nextInt(10));
        }
        return Long.parseLong(number.toString());
    }

}
